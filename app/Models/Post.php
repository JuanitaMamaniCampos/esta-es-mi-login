<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// 
use App\Models\User;
use App\Models\Category;
use App\Models\comment;

class Post extends Model
{
    use HasFactory;

    public function user (){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    } 
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function getRouterKeyName()
    {
        return 'slug';
    }
    public function scopeLatest($query){
        return $query->orderBy('created_at','DESC');
    }
    
}
